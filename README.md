ImgurDownload v1.0

--- **What it does** ---

Download albums and user's entire collections of pictures

--- **Usage** ---

Use the --help command to see a list of commands that can be used

--- **Requirements** ---

* Firefox - Only if ImgDown is set to use Firefox browser with the --browser argument
* Python 2.7
* Selenium 3