import urllib
import urllib2

import os
import sys
import re
import json
import argparse

from functools import partial

# Thread imports
import threading
from multiprocessing.dummy import Pool as ThreadPool

# Selenium imports
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


def download_collection(username, browser):
    """
    Download the entire collection of an user
    :param username: The username of the person whos collection must be downloaded
    :return: None
    """
    
    driver = setup_driver(browser)
    
    driver.get('http://' + username + '.imgur.com/all?')
    
    #  Wait for images to load. If after 90s still not done, crash
    WebDriverWait(driver, 90).until(ec.presence_of_element_located((By.ID, 'account-thumbs')))
    
    urls = get_pages_pic_links(driver)
    
    # Close the webdriver since it nog going to be used anymore
    driver.quit()
    
    download_images(urls, username + '/')


def get_pages_pic_links(driver):
    """
    Get all the image links for all the images in the collection
    :param driver: The webdriver
    :return: A list of all images URLs
    """
    
    success = True
    current_page = 1
    urls = []
    
    while success:
        print 'Getting page %d image links...' % current_page
        
        #  Wait for images to load. If after 90s still not done, crash
        WebDriverWait(driver, 90).until(
            ec.presence_of_element_located((By.XPATH, "//div[contains(@class,'image thumb-title cboxElement')]")))
        
        pics = driver.find_elements_by_xpath("//div[contains(@class,'image thumb-title cboxElement')]")

        urls += build_url_list(pics)
        
        # Go to page the next page if possible
        success = switch_to_page(driver, current_page + 1)
        current_page += 1
    
    return urls


def switch_to_page(driver, page_num):
    """
    Tries to switch to the specified page in the image collection
    :param driver: The webdriver
    :param page_num: The page number to switch to
    :return: True if successfully switched to next page
    """
    
    driver.switch_to_default_content()
    
    elem = driver.find_element_by_class_name('page-numbers')
    
    ass = elem.find_elements_by_xpath(".//*")
    
    for i in xrange(0, len(ass)):
        if int(ass[i].text) == page_num:
            print "Switching to page #%d" % page_num
            
            ass[i].click()
            return True
    
    return False


def download_images(urls, path='pics/', threads=4):
    """
    Download all the images specified in urls
    :param urls: The list of image urls
    :param path: The path where to save the images
    :param threads: The number of threads to be used to download the images
    :return: None
    """
    
    # Create the save directory if it does not exist
    if not os.path.exists(os.getcwd() + "/" + path):
        os.mkdir(os.getcwd() + "/" + path)
    
    # Make the Pool of threads
    thread_pool = ThreadPool(threads)
    
    print "Total number of pics = %d" % len(urls)
    print "Stating downloading of pictures..."
    
    # Create a partial function since pool mapping only supports one argument
    func = partial(download_image, os.getcwd() + "/" + path)
    
    # Open the urls in their own threads and return the results
    thread_pool.map(func, urls)
    
    thread_pool.close()
    thread_pool.join()
    
    print "Downloading complete"


def download_image(path, image_url):
    """
    Download an image with the given URL and save it to path
    :param image_url: The URL from where to download the image
    :param path: The place where to image will be save to
    :return: None
    """

    def progress_indicator(blocknum, blocksize, totalsize):
        """
        
        :param blocknum:
        :param blocksize:
        :param totalsize:
        :return: None
        """
    
        readsofar = blocknum * blocksize
        if totalsize > 0:
            percent = readsofar * 1e2 / totalsize
            s = "\r%5.1f%% %*d / %d" % (
                percent, len(str(totalsize)), readsofar, totalsize)
            sys.stderr.write(s)
            if readsofar >= totalsize:  # near the end
                sys.stderr.write("\n")
        else:  # total size is unknown
            sys.stderr.write("read %d\n" % (readsofar,))
    
    parts = image_url.split('/')
    file_path = path + parts[len(parts) - 1]
    
    print "Thread %d: Checking %s..." % (threading.current_thread().ident, parts[len(parts) - 1])
    
    if not os.path.isfile(file_path):  # Check that the file does not already exist
        print 'Thread %d: Downloading %s' % (threading.current_thread().ident, parts[len(parts) - 1])
        
        # Download the image
        # print threading.activeCount() # TODO: This count is not working, replace with something else
        if threading.activeCount() > 1:
            # If there is more than one thread downloading, don't give a report hook. It just does not work visually
            urllib.urlretrieve(image_url, file_path)
        else:
            urllib.urlretrieve(image_url, file_path, reporthook=progress_indicator)
    else:  # The file already exists, check that the sizes are the same
        print "Thread %d: Imgur Filesize = %d" % (
            threading.current_thread().ident, int(urllib.urlopen(image_url).info()['Content-Length']))
        print "Thread %d: System Filesize = %d" % (threading.current_thread().ident, int(os.stat(file_path).st_size))
        
        if int(urllib.urlopen(image_url).info()['Content-Length']) != int(os.stat(file_path).st_size):
            print 'Thread %d: Downloading %s' % (threading.current_thread().ident, parts[len(parts) - 1])
            
            # Download the image
            if threading.activeCount() > 1:
                # If there is more than one thread downloading, don't give a report hook. It just does not work visually
                urllib.urlretrieve(image_url, file_path)
            else:
                urllib.urlretrieve(image_url, file_path, reporthook=progress_indicator)
        else:
            print "Thread %d: Skipping %s" % (threading.current_thread().ident, parts[len(parts) - 1])


def build_url_list(pics):
    """
    
    :param pics:
    :return:
    """
    
    urls = []
    
    for pic in pics:
        print pic.get_attribute("id")
        
        urls.append("http://i.imgur.com/" + pic.get_attribute("id") + ".jpg")
    
    return urls


def setup_driver(browser='phantom'):
    """
    Creates an instance of the desired webdriver
    :param driver: The browser which must be used
    :return: An instance of a webdriver
    """
    
    print 'Setting up driver'
    
    if browser == 'firefox':
        return webdriver.Firefox()
    elif browser == 'phantom':
        return webdriver.PhantomJS()
    else:
        print('ERROR: This browser is not yet supported')
        exit()


def download_albums(album_names):
    """
    Download the albums specified in the arguments
    :param album_names: The names of the albums to download
    :return: None
    """
    
    id = '7bec07816d35ea5'
    
    for i in xrange(0, len(album_names)):
        print('Processing album %s...' % album_names[i])
        
        pic_urls = []
        
        url = 'https://api.imgur.com/3/album/' + album_names[i] + '/images'
        req = urllib2.Request(url, None, {"Authorization": "Client-ID %s" % id})
        
        response = urllib2.urlopen(req)
        json_data = json.loads(response.read())
        
        data = json_data[u'data']
        
        for d in data:
            pic_urls.append(d[u'link'])
        
        download_images(pic_urls, path='album-' + album_names[i] + "/")
        
        print('-------------------------------------------------------------------------')


def parse_args():
    """
    Parse the command line arguments
    :return: The parsed arguments
    """

    parser = argparse.ArgumentParser(description='Kills you IRL', add_help=True, usage=usage(), version='1.0')
    
    parser.add_argument('--collect', '-C', type=str, help='Names of accounts whose entire collection must be download. Note that the person must have made the collection publicly available')
    parser.add_argument('--browser', '-B', type=str, choices=['phantom', 'firefox'], default='phantom', help='The browser to use when "--collect" is used')
    parser.add_argument('-album', '-A', type=str, help='The names of albums to download')

    arguments = parser.parse_args()
    
    print arguments
    
    return arguments


def decide_download_action(arguments):
    """
    Take appropriate actions
    :return: None
    """
    
    # Download all albums specified
    if arguments.album:
        albums = arguments.album.split(',')
        print 'Albums = ', albums
        
        download_albums(albums)
        
    # Download all collections specified
    if arguments.collect:
        collects = arguments.collect.split(',')
        print 'Collections = ', collects
        
        for collect in collects:
            download_collection(collect, arguments.browser)


def usage():
    """
    A function that returns a message on how to use the script
    :return: The help message
    """
    
    return 'Use -h for a list of commands\n' \
           '\nExample: Download albums bHdaF and O3Y8S\n' \
           'python .\\ImgDown.py --album bHdaF,O3Y8S\n' \
           '\nExample: Download albums bHdaF and O3Y8S and user rockgoesnightcore collection\n' \
           'python .\\ImgDown.py --album bHdaF,O3Y8S --all rockgoesnightcore\n'


if __name__ == '__main__':
    arguments = parse_args()
    decide_download_action(arguments)
